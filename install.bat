@echo off
title Discordia Installer (Unofficial)
:luvit
if exist luvit.exe goto ffmpeg
echo Downloading and installing dependencies...
PowerShell -NoProfile -ExecutionPolicy unrestricted -Command "[Net.ServicePointManager]::SecurityProtocol = 'Tls12'; iex ((new-object net.webclient).DownloadString('https://github.com/luvit/lit/raw/master/get-lit.ps1'))"
:ffmpeg
if exist ffmpeg.exe goto discordia
PowerShell -Command "curl -o ffplay.exe https://www.dropbox.com/s/oxe4sgwvey1h1b3/ffplay.exe?dl=1"
PowerShell -Command "curl -o ffmpeg.exe https://www.dropbox.com/s/jh29u63zd6p4t5m/ffmpeg.exe?dl=1"
PowerShell -Command "curl -o ffprobe.exe https://www.dropbox.com/s/di4e5paf5fh4efb/ffprobe.exe?dl=1"
:discordia
lit install SinisterRectus/discordia
echo Discordia has finished installing. You may now start coding!
pause && exit